#include <IRremote.h>
#include <EEPROM.h>
#include "FastSPI_LED2.h"

struct CRGB {
  byte g;
  byte r;
  byte b; };
  
enum action {
  STATIC_COLOR,
  RAINBOWFADE
};

/* CONFIGURATION */
const int NUM_LEDS   = 203;
const int RELAIS_PIN =  8;
const int RECV_PIN   = 10;

const int eepr_addr_r = 0;
const int eepr_addr_g = 1;
const int eepr_addr_b = 2;
const int eepr_addr_brightness = 3;

/* OBJECTS/VARIABLES */
IRrecv irrecv(RECV_PIN);
decode_results results;

WS2811Controller800Mhz<12> LED;
struct CRGB leds[NUM_LEDS];
struct CRGB currentColor;

int ihue   = 0;

action currentAction = STATIC_COLOR;
int brightnessDivider   = 6;
int effectDelay         = 100;

int iRelaisState        = HIGH;

/* 
 * Helper Functions 
 */
 
 
void increaseBrightness(void) {
  if( brightnessDivider > 1 )
    brightnessDivider -= 1;
  else
    brightnessDivider = 1;
    
  EEPROM.write(eepr_addr_brightness, brightnessDivider);
}


void decreaseBrightness(void) {
 if( brightnessDivider < 10 )
   brightnessDivider += 1;
 else
   brightnessDivider = 10;
   
 EEPROM.write(eepr_addr_brightness, brightnessDivider);
}


void writeColorToEEPROM( struct CRGB color ) {
  EEPROM.write(eepr_addr_r, color.r);
  EEPROM.write(eepr_addr_g, color.g);
  EEPROM.write(eepr_addr_b, color.b);
}


struct CRGB readColorFromEEPROM(void) {
  struct CRGB color;
  color.r = EEPROM.read(eepr_addr_r);
  color.g = EEPROM.read(eepr_addr_g);
  color.b = EEPROM.read(eepr_addr_b);
}


//-CONVERT HSV VALUE TO RGB
void HSVtoRGB(int hue, int sat, int val, int colors[3]) {
	// hue: 0-359, sat: 0-255, val (lightness): 0-255
	int r, g, b, base;

	if (sat == 0) { // Achromatic color (gray).
		colors[0]=val;
		colors[1]=val;
		colors[2]=val;
	} else  {
		base = ((255 - sat) * val)>>8;
		switch(hue/60) {
			case 0:
				r = val;
				g = (((val-base)*hue)/60)+base;
				b = base;
				break;
			case 1:
				r = (((val-base)*(60-(hue%60)))/60)+base;
				g = val;
				b = base;
				break;
			case 2:
				r = base;
				g = val;
				b = (((val-base)*(hue%60))/60)+base;
				break;
			case 3:
				r = base;
				g = (((val-base)*(60-(hue%60)))/60)+base;
				b = val;
				break;
			case 4:
				r = (((val-base)*(hue%60))/60)+base;
				g = base;
				b = val;
				break;
			case 5:
				r = val;
				g = base;
				b = (((val-base)*(60-(hue%60)))/60)+base;
				break;
		}

		colors[0]=r;
		colors[1]=g;
		colors[2]=b;
	}
}

 
//-SET THE COLOR OF A SINGLE RGB LED
void set_color_led(int adex, int colorR, int colorG, int colorB) {    
  leds[adex].r = (int)(colorR / brightnessDivider);
  leds[adex].g = (int)(colorG / brightnessDivider);
  leds[adex].b = (int)(colorB / brightnessDivider);  
}

void set_color_ledstruct(int adex, struct CRGB color) {    
  set_color_led( adex, color.r, color.g, color.b ); 
}
  
  
//-SET ALL LEDS TO ONE COLOR
void one_color_all( struct CRGB color ) { 
    for(int i = 0 ; i < NUM_LEDS; i++ ) {
      set_color_ledstruct(i, color);
    }
  LED.showRGB((byte*)leds, NUM_LEDS);  
}


//- RAINBOW FADING
void rainbow_fade(int idelay) { //-FADE ALL LEDS THROUGH HSV RAINBOW
    ihue++;
    if (ihue >= 359) {ihue = 0;}
    int thisColor[3];
    HSVtoRGB(ihue, 255, 255, thisColor);
    for(int idex = 0 ; idex < NUM_LEDS; idex++ ) {
      set_color_led(idex,thisColor[0],thisColor[1],thisColor[2]); 
    }
    
    LED.showRGB((byte*)leds, NUM_LEDS);    
    delay(idelay);
}


/* 
 * MAIN CODE
 */
 
void setup()
{
  //Serial.begin(9600);
  pinMode(RELAIS_PIN, OUTPUT);   
  digitalWrite(RELAIS_PIN, iRelaisState);
  LED.init();
  irrecv.enableIRIn();
  
  currentColor       = readColorFromEEPROM();
  brightnessDivider  = EEPROM.read(eepr_addr_brightness);
  one_color_all( currentColor );
}

void loop() {
  if (irrecv.decode(&results)) {
    //Serial.println(results.value, HEX);
    
    switch( results.value )
    {
     /* Row 1 */
     case 0xFF3AC5:
       increaseBrightness();
       break;
       
     case 0xFFBA45:
       decreaseBrightness();
       break;
       
     case 0xFF827D:
       currentAction  = STATIC_COLOR;
       currentColor.r = 0;
       currentColor.g = 0;
       currentColor.b = 0;
       break;
       
     case 0xFF02FD:
       if (iRelaisState == LOW)
         iRelaisState = HIGH;
       else
         iRelaisState = LOW;
       digitalWrite(RELAIS_PIN, iRelaisState);     
       break;
       
       
     /* Row 2 */
     case 0xFF1AE5:
       currentAction  = STATIC_COLOR;
       // red
       currentColor.r = 255;
       currentColor.g = 0;
       currentColor.b = 0;
       break;
       
     case 0xFF9A65:
       currentAction  = STATIC_COLOR;
       // green
       currentColor.r = 0;
       currentColor.g = 255;
       currentColor.b = 0;
       break;
       
     case 0xFFA25D:
       currentAction  = STATIC_COLOR;
       //blue
       currentColor.r = 0;
       currentColor.g = 0;
       currentColor.b = 255;
       break;
       
     case 0xFF22DD:
       currentAction  = STATIC_COLOR;
       // white
       currentColor.r = 255;
       currentColor.g = 255;
       currentColor.b = 255;
       break;
       
       
     /* Row 3 */
     case 0xFF2AD5:
       break;
       
     case 0xFFAA55:
       break;
       
     case 0xFF926D:
       break;
       
     case 0xFF12ED:
       break;
       
       
     /* Row 4 */
     case 0xFF0AF5:
       break;
       
     case 0xFF8A75:
       break;
       
     case 0xFFB24D:
       break;
       
     case 0xFF32CD:
       break;
       
       
     /* Row 5 */
     case 0xFF38C7:
       break;
     //button unused
     case 0xFF7887:
       break;
     //button unused
     
     
     /* Row 6 */
     case 0xFF18E7:
       break;
     //button unused
     case 0xFF58A7:
       break;
     //button unused
     
     
    /* Row 7 */
     //button unused
     case 0xFFA857:
       break;
     case 0xFF6897:
       break;
     //button unused
     
     
    /* Row 8 */
     //button unused
     case 0xFF8877:
       break;
     case 0xFF48B7:
       break;
     //button unused
     
     
    /* Row 9 */
     //button unused
     case 0xFFB04F:
       break;     
     case 0xFF708F:
       break;
     //button unused
     
     
    /* Row 10 */    
     //button unused
     case 0xFF906F:
       break;
     case 0xFF50AF:
       break;
     //button unused
     
     
    /* Row 11 */
     //button unused
     case 0xFFA05F:
       break;
     case 0xFF609F:
       currentAction = RAINBOWFADE;
       break;
     //button unused
     
     default:
       break;     
      
    }   
    
    switch( currentAction ) {
     
     case STATIC_COLOR:
       one_color_all(currentColor);
       break;
       
     case RAINBOWFADE:
       rainbow_fade( effectDelay );
       break;
      
     default:
       break; 
    }
    
    irrecv.resume(); // Receive the next value
  }
}
